# 3' UTR from S. pennellii

We need 500bp of CDS and 500bp of 3'UTR for mapping of Brad's DGE sequences to S.penn.

The Repository shows how I made the sequence file and has the sequences available for download

## Download

If you are just wanting the sequences, [click here](https://bitbucket.org/jnmaloof/s_pennelli_3prime/downloads/Spenn500.500.3primeUTR.mergeNames.031315.fa.gz)

## Extract sequences

Download sequences and annotation

        wget ftp://ftp.solgenomics.net/genomes/Solanum_pennellii/Spenn.fasta
        wget ftp://ftp.solgenomics.net/genomes/Solanum_pennellii/spenn_v2.0_gene_models_annot.gff
        wget ftp://ftp.solgenomics.net/genomes/Solanum_pennellii/annotations/Spenn-v2-cds-annot.fa

Download Mike's 3' UTR extractor

        git clone https://github.com/mfcovington/extract-utr.git

Do the extractions

        perl extract-utr/extract-utr.pl --threeprime --gff_file spenn_v2.0_gene_models_annot.gff --cds_fa_file Spenn-v2-cds-annot.fa --genome_fa_file Spenn.fasta

Change the name of the output file

        mv out.fa Spenn500.500.3primeUTR.fa
        
Do some checks

        grep -c ">" Spenn500.500.3primeUTR.fa
        grep -c ">" Spenn-v2-cds-annot.fa
        grep -c "mRNA" spenn_v2.0_gene_models_annot.gff
        
Looks good.

## Change names

Problem is that S.penn genes have their own names and we want to know which S.lyc genes these correspond to.

I emailed Tony Bolger to get the translation table (the one from the S. pennellii genome paper is too old)

Use `merge_names.R` (from this repos) to add S.lyc names to the S.penn sequences